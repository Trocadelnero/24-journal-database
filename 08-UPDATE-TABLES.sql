-- AUTHOR UPDATE
UPDATE author SET first_name = 'STEPHEN' WHERE author.id=1; 
UPDATE author SET last_name = 'KING' WHERE author.id=1; 
UPDATE author SET first_name = 'DEAN' WHERE author.id=2;
-- JOURNAL UPDATE
UPDATE journal SET title = "JavaScript!" WHERE id = 1;
UPDATE journal SET title = "Personal!" WHERE id = 2;
UPDATE journal SET title = "Neighbour Related" WHERE id = 3;
-- TAG UPDATE 
UPDATE tag SET title = "JavaScript"  WHERE id = 1;
UPDATE tag SET title = "Changes"  WHERE id = 2;
UPDATE tag SET title = "Discovery" WHERE id = 3;
-- JOURNAL ENTRY UpDATE
UPDATE journal_entry SET title = "Sequelize It!" WHERE id = 2;
UPDATE journal_entry  SET author_id = 2  WHERE id = 3;
UPDATE journal_entry  SET content = 'I do not sleep enough. My neighbours keep me up', author_id = 2  WHERE id = 2;



