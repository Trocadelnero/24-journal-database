USE db_journal_entries;

-- TAG MOCK DATA
INSERT INTO tag (title) VALUES ('javascript');
INSERT INTO tag (title) VALUES ('changes');
INSERT INTO tag (title) VALUES ('discovery');
INSERT INTO tag (title) VALUES ('programming');
INSERT INTO tag (title) VALUES ('sleep');
INSERT INTO tag (title) VALUES ('mysql');
-- SELECT * FROM tag;