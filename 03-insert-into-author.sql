INSERT INTO author (first_name, last_name, created_at)
VALUES ("Stephen", "King", "1988-06-16");

INSERT INTO author (first_name, last_name, created_at)
VALUES ("Dean", "Koontz", "1988-12-31");

INSERT INTO author (first_name, last_name, created_at)
VALUES ("Edgar Allan", "Poe", "1849-10-06");