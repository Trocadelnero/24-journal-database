-- JOURNAL_TAG MOCK DATA
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (1, 1);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (1, 4);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (2, 2);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (2, 5);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (3, 3);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (4, 6);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (4, 4);
INSERT INTO journal_tag (journal_entry_id, tag_id) VALUES (5, 1);
SELECT * FROM journal_tag;