USE db_journal_entries;
-- JOURNAL ENTRY MOCK DATA
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('JavaScript Factory', 'Today I learned about JavaScript factory pattern. It was great', 1, 1);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('Sequelize', 'Learning how to setup sequelize in a Node-Express application', 1, 1);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('I need sleep', 'I do not sleep enough. I stay up too late and forget the time. I am tired', 1, 2);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('Vampires', 'Today we discovered shiny vampires. They were very emotional. Weird...', 3, 3);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('MySQL Joins', 'MySQL Joins are weird. So much to remember. :(', 1, 4);
SELECT * FROM journal_entry;