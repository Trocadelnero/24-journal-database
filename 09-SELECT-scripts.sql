-- SELECT journal title,content author, title, tags. Concat and group
SELECT journal_entry.title, journal_entry.content, 
CONCAT (author.first_name, ' ', author.last_name) AS author_name, journal.title,
GROUP_CONCAT(tag.title) AS tags
FROM db_journal_entries.journal_entry
INNER JOIN author ON author.id = author_id
INNER JOIN journal ON journal.id = journal_id
LEFT JOIN journal_tag ON journal_tag.journal_entry_id = journal_entry.id
LEFT JOIN tag ON journal_tag.tag_id = tag.id
GROUP BY journal_entry.id;

-- SELECT Title, concat author , tags as category, group by title
SELECT journal_entry.title as Title,
CONCAT (author.first_name, ' ', author.last_name) AS Author,
GROUP_CONCAT(tag.title) AS Category 
FROM journal_entry 
LEFT JOIN journal_tag ON journal_tag.journal_entry_id = journal_entry.id
LEFT JOIN tag ON tag.id = journal_tag.tag_id 
LEFT JOIN author ON author.id = journal_entry.author_id
GROUP BY journal_entry.title;



-- SELECT Entries set titles 
SELECT COUNT(journal_entry.id) AS Entries, journal.title AS TypeOf
FROM journal_entry
LEFT JOIN journal ON journal_entry.journal_id = journal.id
WHERE journal.id = 1;
