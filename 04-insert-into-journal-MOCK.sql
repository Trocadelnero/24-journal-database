USE db_journal_entries;

-- JOURNAL MOCK DATA
INSERT INTO journal (title) VALUES ('JavaScript');
INSERT INTO journal (title) VALUES ('Personal');
INSERT INTO journal (title) VALUES ('Work');
INSERT INTO journal (title) VALUES ('Databases');
SELECT * FROM journal;
